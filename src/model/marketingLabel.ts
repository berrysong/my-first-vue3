/*
 * @Descripttion: 
 * @version: 
 * @Author: Jf W
 * @Date: 2021-08-23 09:46:36
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-08-23 09:48:17
 */
export interface MarketingLabelModel {
  id: number,
  title: string,
  number: number
}