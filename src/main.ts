/*
 * @Descripttion: 
 * @version: 
 * @Author: Jf W
 * @Date: 2021-07-22 11:15:27
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-08-18 10:53:22
 */
import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css'
import App from './App.vue'
import router from "./router/router"
import './style/index.scss'

const app = createApp(App)
app.use(router)
app.use(ElementPlus)
app.mount('#app')