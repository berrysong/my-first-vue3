/*
 * @Descripttion: 
 * @version: 
 * @Author: Jf W
 * @Date: 2021-07-22 11:15:27
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-08-24 10:58:36
 */
import { createRouter, createWebHistory } from "vue-router";
// createWebHistory 为 history mode
// createWebHashHistory 为 hash mode


const routes = [
  {
    path: '/',
    redirect: "/login"
  },
  {
    path: "/login",
    name: "Login",
    component: () => import('../pages/login/Login.vue')
  },
  {
    path: '/management',
    name: 'Management',
    component: () => import('../pages/managemanet/index.vue')
  },
  {
    path: '/switch',
    name: 'Switch',
    component: () => import('../components/common/Switch.vue')
  },
  {
    path: '/steps',
    name: 'Steps',
    component: () => import('../components/common/Steps.vue')
  },
  {
    path: '/header',
    name: 'Header', // 头部
    component: () => import('../components/common/headerTop/HeaderTop.vue')
  },
  {
    path: '/stuffInfo',
    name: 'StuffInfo', // 员工信息弹层
    component: () => import('../components/business/StuffInfoDialog.vue')
  },
  {
    path: '/addSource',
    name: 'AddSource', // 增加资源
    component: () => import('../components/business/AddSource.vue')
  },
  {
    path: '/leftNavigation',
    name: 'LeftNavigation',
    component: () => import('../components/common/leftNav/LeftNavigation.vue')
  },
  {
    path: '/index',
    name: 'Index',
    component: () => import('../pages/index/Index.vue')
  },
  {
    path: '/sourcePicture',
    name: 'SourceFirstPicture',
    component: () => import('../components/business/SourceFirstPicture.vue')
  },
  {
    path: '/batchOperation',
    name: 'BatchOperation',
    component: () => import('../components/business/BatchOperation.vue')
  },
  {
    path: '/table',
    name: 'TableList',
    component: () => import('../components/business/TableList.vue')
  },
  {
    path: '/enclosure',
    name: 'Enclosure',
    component: () => import('../components/business/Enclosure.vue')
  },
  {
    path: '/tabs',
    name: 'Tabs',
    component: () => import('../components/common/tabs/Tabs.vue')
  },
  {
    path: '/listTabs',
    name: 'ListTabs',
    component: () => import('../components/common/tabs/ListTabs.vue')
  },
  {
    path: '/search',
    name: 'AdvancedSearch',
    component: () => import('../components/common/AdvancedSearch.vue')
  },
  {
    path: '/marketingLabel',
    name: 'MarketingLabel',
    component: () => import('../components/business/marketingLabel/MarketingLabel.vue')
  },
  { 
    path: '/test',
    name: 'Test',
    component: () => import('../components/common/Test.vue')
  },
  {
    path: '/visaList',
    name: 'VisaList',
    component: () => import('../pages/visaSource/VisaList.vue')
  }
]

export default createRouter({
  history: createWebHistory(),
  routes
})